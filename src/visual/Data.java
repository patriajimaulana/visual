/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visual;

/**
 *
 * @author patriaji
 */
public class Data {
   private String username, name, sex, background, password;
   
   public Data(String username, String name, String sex, String background, String password){
     this.username = username;
     this.name = name;
     this.sex = sex;
     this.background = background;
     this.password = password;
   }
   
   public String getUsername(){
      return username; 
   }
   public String getName(){
      return name; 
   }
   public String getSex(){
      return sex; 
   }
   public String getBackground(){
      return background; 
   }
   public String getPassword(){
      return password; 
   }
   
    
}
